/*
 * hash.h
 *
 * $Id: hash.h 62 2017-08-28 21:38:50Z diraison $
 *
 * This file is part of the btcheck project (c) 2008-2009 distributed
 * under the GNU GPLv3 license and created by Jean Diraison
 * <jean.diraison@ac-rennes.fr>
 *
 * URL: http://sourceforge.net/projects/btcheck/
 *
 */

#ifndef BTCHECK_HASH_H_
#define BTCHECK_HASH_H_

#ifdef WITH_OPENSSL
#include <openssl/sha.h>
#endif

#ifndef SHA_DIGEST_LENGTH
#define SHA_DIGEST_LENGTH		(20)
#endif

#ifdef WITH_POLARSSL
#include <mbedtls/sha1.h>
#define SHA_CTX mbedtls_sha1_context
#endif

#ifdef WITH_GNUTLS
#include <gnutls/gnutls.h>
#include <gnutls/crypto.h>
#define SHA_CTX struct { \
	gnutls_hash_hd_t dig; \
}
#endif

#ifdef WITH_GCRYPT
#include <gcrypt.h>
#define SHA_CTX struct { \
	gcry_md_hd_t handler; \
}
#endif

#ifdef WITH_TOMCRYPT
#include <tomcrypt.h>
#define SHA_CTX hash_state
#endif

#ifdef WITH_NETTLE
#include <nettle/sha1.h>
#define SHA_CTX struct sha1_ctx
#endif

#ifdef WITH_BEECRYPT
#include <beecrypt/sha1.h>
#define SHA_CTX sha1Param
#endif

#ifdef WITH_KERNEL_CRYPTO_API
#include "kernelcryptoapi.h"
#define SHA_CTX lkca_hash_ctx
#endif

#ifdef WITH_WINDOWS_CRYPTOAPI
#include <windows.h>
#include <wincrypt.h>
#define SHA_CTX struct { \
	HCRYPTPROV hCryptProv; \
	HCRYPTHASH hCryptHash; \
}
#endif

#ifndef SHA_CTX
#ifndef WITH_OPENSSL
#include <stdint.h>
#define SHA_CTX struct { \
	uint32_t       hash[5]; \
	long long      length; \
	int            buflen; \
	unsigned char  buffer[64]; \
}
#define WITH_BUILTIN_SHA
#endif
#endif

#define HASH_LENGTH		(SHA_DIGEST_LENGTH)

typedef SHA_CTX hashcharctx_t;

int init_hash_char(hashcharctx_t *ctx);
int update_hash_char(hashcharctx_t *ctx, int c);
int final_hash_char(unsigned char *md, hashcharctx_t *ctx);
unsigned char *compute_hash_buffer(unsigned char *md, void *data, unsigned long length);

#endif
