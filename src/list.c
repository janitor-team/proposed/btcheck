/*
 * list.c
 *
 * $Id: list.c 26 2012-02-19 22:28:17Z diraison $
 *
 * This file is part of the btcheck project (c) 2008-2009 distributed
 * under the GNU GPLv3 license and created by Jean Diraison
 * <jean.diraison@ac-rennes.fr>
 *
 * URL: http://sourceforge.net/projects/btcheck/
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include "btree.h"
#include "meta.h"

void print_files_list(btree_t *torrentbtree)
{
	btree_t	*torrentinfo, *torrentinfofiles;
	string_t torrentinfoname;
	integer_t torrentinfolength;
	int ret;

	/* Name */
	ret = get_meta_string(torrentbtree, TorrentInfoNameSearch, &torrentinfoname, NULL);
	if (ret != 0) {
		fprintf(stderr, "Can't get name!\n");
		exit(EXIT_FAILURE);
	}
	/* Info Files */
	torrentinfofiles = search_dict_value(torrentbtree, TorrentInfoFilesSearch);
	if (torrentinfofiles == NULL) {		/* single file */
		torrentinfolength = 0;
		ret = get_meta_integer(torrentbtree, TorrentInfoLengthSearch, &torrentinfolength);
		if (ret != 0) {
			fprintf(stderr, "Can't get length!\n");
			exit(EXIT_FAILURE);
		}
		fprintf(stdout, "%s\t(" LLD_FMT ")\n", (char *)torrentinfoname, (long long)torrentinfolength);
	} else {				/* multi file */
		fprintf(stderr, "Base directory : %s\n", (char *)torrentinfoname);
		torrentinfofiles = search_dict_value(torrentbtree, TorrentInfoFilesSearch);
		if (torrentinfofiles == NULL) {
			fprintf(stderr, "Can't get files!\n");
			exit(EXIT_FAILURE);
		}
		ret = map_list(torrentinfofiles, (int (*)(void *, btree_t *))print_path_map, stdout);
		if (ret != 0) {
			fprintf(stderr, "Can't print files.\n");
			exit(EXIT_FAILURE);
		}
	}
	return;
}
