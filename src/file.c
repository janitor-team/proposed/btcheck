/*
 * file.c
 *
 * $Id: file.c 36 2013-10-04 22:28:26Z diraison $
 *
 * This file is part of the btcheck project (c) 2008-2009 distributed
 * under the GNU GPLv3 license and created by Jean Diraison
 * <jean.diraison@ac-rennes.fr>
 *
 * URL: http://sourceforge.net/projects/btcheck/
 *
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "btcheck.h"
#include "btree.h"
#include "file.h"
#include "meta.h"

#define MIN(x,y)	((x) < (y) ? (x) : (y))

btree_t 	*TorrentInfoFiles;
string_t	TorrentInfoName;
integer_t 	TorrentInfoLength;


static int open_next(btfile_t *btfile)
{
	btree_t *btree, *lengthbtree, *pathlist, *element;
	integer_t length;
	string_t string;
	int i, ret;

	if (btfile == NULL || btfile->list == NULL)
		return -1;
	if (btfile->index >= (get_btree_length(btfile->list) - 1))
		return -1;
	if (btfile->file != NULL) {
		ret = fgetc(btfile->file);
		if (ret != EOF) {
			fprintf(stderr, "\rError file size too big!\n");
		}
		ret = fclose(btfile->file);
		if (ret != 0) {
			fprintf(stderr, "\rError closing previous file (%s), continue anyway...\n", strerror(errno));
		}
		btfile->file = NULL;
	}
#ifdef HAVE_FCHDIR
	ret = fchdir(btfile->dirfd);
#else
	ret = chdir(btfile->dirname);
#endif
	if (ret != 0) {
		perror("Can't return to base directory.");
		exit(EXIT_FAILURE);
	}
	btfile->btferr = BTFERR_NONE;
	btfile->size = 0;
	btfile->index++;

	btree = get_list_element(btfile->list, btfile->index);
	if (btree == NULL) {
		fprintf(stderr, "Can't get list element with index %d.\n", btfile->index);
		exit(EXIT_FAILURE);
	}
	
	lengthbtree = search_dict_value(btree, LengthSearch);
	if (lengthbtree == NULL) {
		fprintf(stderr, "ERROR : no length attribute found!\n");
		exit(EXIT_FAILURE);
	}
	get_btree_integer(lengthbtree, &length);
	btfile->size = length;
	
	pathlist = search_dict_value(btree, PathSearch);
	if (pathlist == NULL) {
		fprintf(stderr, "\rCan't get path from list element at index %d.\n", btfile->index);
		return -1;
	}
	if (Verbose >= 1)
		printf("\rOpening file: ");
	for (i = 0; i < get_btree_length(pathlist); i++) {
		element = get_list_element(pathlist, i);
		if (element != NULL) {
			get_btree_string(element, &string, NULL);
			if (i < (get_btree_length(pathlist) - 1)) {
				if (Verbose >= 1)
					printf("%s/", string);
				ret = 0;
				if (strlen(string) != 0)
					ret = chdir(string);
				if (ret != 0) {
					fprintf(stderr, "\rCan't change directory to %s (%s).\n", string, strerror(errno));
					return -1;
				}
			} else {
				if (Verbose >= 1)
					printf("%s\n", string);
				btfile->file = fopen(string, "rb");
				if (btfile->file == NULL) {
					fprintf(stderr, "\rCan't open file %s (%s).\n", string, strerror(errno));
					return -1;
				}
			}
		} else {
			return -1;
		}
	}
	return 0;
}

btfile_t *open_btfile(btree_t *torrentbtree)
{
	btfile_t *btfile;
	int ret;

	if (torrentbtree == NULL)
		return NULL;
	ret = get_meta_string(torrentbtree, TorrentInfoNameSearch, &TorrentInfoName, NULL);
	if (ret != 0) {
		fprintf(stderr, "Can't get name!\n");
		exit(EXIT_FAILURE);
	}
	btfile = malloc(sizeof(btfile_t));
	if (btfile == NULL) {
		fprintf(stderr, "Can't create new btfile.\n");
		exit(EXIT_FAILURE);
	}
	TorrentInfoFiles = search_dict_value(torrentbtree, TorrentInfoFilesSearch);
	if (TorrentInfoFiles == NULL) {		/* single file torrent */
#ifdef HAVE_FCHDIR
		btfile->dirfd = -1;
#else
		btfile->dirname[0] = '\0';
#endif
		btfile->index = 0;
		btfile->list = NULL;
		btfile->file = fopen(TorrentInfoName, "rb");
		if (btfile->file == NULL) {
			fprintf(stderr, "Can't open file \"%s\" (%s).\n", TorrentInfoName, strerror(errno));
			exit(EXIT_FAILURE);
		}
		ret = get_meta_integer(torrentbtree, TorrentInfoLengthSearch, &TorrentInfoLength);
		if (ret != 0) {
			fprintf(stderr, "Can't get info length.\n");
			exit(EXIT_FAILURE);
		}
		btfile->size = TorrentInfoLength;
	} else {				/* multi file torrent */
		ret = chdir(TorrentInfoName);
		if (ret != 0)
			if (Verbose >= 0)
				fprintf(stderr, "\rCan't change current directory to \"%s\" (%s), continue anyway...\n", TorrentInfoName, strerror(errno));
#ifdef HAVE_FCHDIR
		btfile->dirfd = open(".", O_RDONLY);
		ret = btfile->dirfd;
#else
		char *retdirname = getcwd(btfile->dirname, PATH_MAX-1);
		ret = (retdirname == NULL ? -1 : 0);
#endif
		if (ret == -1) {
			fprintf(stderr, "Can't open current directory (%s).\n", strerror(errno));
			exit(EXIT_FAILURE);
		}
		btfile->index = -1;
		btfile->list = TorrentInfoFiles;
		btfile->file = NULL;
		ret = open_next(btfile);
		if (ret != 0) {
			fprintf(stderr, "\rCan't open first file (%s).\n", strerror(errno));
			// exit(EXIT_FAILURE);
		}
	}
	
	return btfile;
}

size_t read_btfile(btfile_t *btfile, void *data, size_t length)
{
	size_t size, minlen, totallen = 0;
	int ret, readerr = 0;

	if (btfile == NULL)
		return READERR_NOFILE;
	while (length > 0) {
		while (btfile->size == 0) {
			ret = open_next(btfile);
		}
		minlen = MIN(length, btfile->size);
		size = 0;
		if (btfile->file != NULL) {
			clearerr(btfile->file);
			size = fread(data, 1, minlen, btfile->file);
		}
		if (size == 0) {
			if (btfile->file != NULL && feof(btfile->file)) {
				if (!(btfile->btferr & BTFERR_TOOSHORT)) {
					fprintf(stderr, ZeroFill ? "\rError file size too short, continue filling with zeros...\n" : "\rError file size too short, continue anyway...\n");
					btfile->btferr |= BTFERR_TOOSHORT;
				}
			} else {
				if (!(btfile->btferr & BTFERR_READ)) {
					fprintf(stderr, ZeroFill ? "\rRead btfile error, continue filling with zeros...\n" : "\rRead btfile error, continue anyway...\n");
					btfile->btferr |= BTFERR_READ;
				}
			}
			if (ZeroFill)
				memset(data, 0, minlen);
			else
				readerr = 1;
			size = minlen;
		}
		btfile->size -= size;
		length       -= size;
		data         += size;
		totallen     += size;
	}
	return readerr == 0 ? totallen : READERR_HOLES;
}

int close_btfile(btfile_t *btfile)
{
	int ret = -1;
	if (btfile == NULL)
		return -1;
	if (btfile->file != NULL) {
		ret = fgetc(btfile->file);
		if (ret != EOF) {
			fprintf(stderr, "Error file size too big!\n");
		}
		ret = fclose(btfile->file);
	}
	free(btfile);
	return ret;
}
