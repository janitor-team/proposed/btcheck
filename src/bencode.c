/*
 * bencode.c
 *
 * $Id: bencode.c 42 2015-05-31 21:50:09Z diraison $
 *
 * This file is part of the btcheck project (c) 2008-2009 distributed
 * under the GNU GPLv3 license and created by Jean Diraison
 * <jean.diraison@ac-rennes.fr>
 *
 * URL: http://sourceforge.net/projects/btcheck/
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "btree.h"
#include "bdecode.h"

static void bencode_integer(integer_t integer, void (*func)(void *, int), void *ctx)
{
	int buffer[MAX_INTEGER_DIGITS+1];
	int i, sign;
	i = 0;
	sign = 1;
	if (integer < 0) {
		sign = -1;
		integer = -integer;
	}
	do {
		buffer[i++] = (integer % 10)+'0';
		integer = integer / 10;
	} while (integer != 0);
	if (sign < 0)
		func(ctx, '-');
	while (--i >= 0)
		func(ctx, buffer[i]);
}

static void bencode_string(string_t string, int length, void (*func)(void *, int), void *ctx)
{
	int i;
	bencode_integer((integer_t)length, func, ctx);
	func(ctx, ':');
	for (i = 0; i < length; i++)
		func(ctx, (int)((unsigned char *)string)[i]);
}

void bencode(btree_t *btree, void (*func)(void *, int), void *ctx)
{
	integer_t integer;
	string_t string;
	btree_t *tmpbtree;
	int i, length;
	if (btree == NULL)
		return;
	switch (get_btree_type(btree)) {
	case BTYPE_INTEGER:	func(ctx, 'i');
				get_btree_integer(btree, &integer);
				bencode_integer(integer, func, ctx);
				func(ctx, 'e');
				break;
	case BTYPE_STRING:	get_btree_string(btree, &string, &length);
				bencode_string(string, length, func, ctx);
				break;
	case BTYPE_LIST:	func(ctx, 'l');
				for(i = 0; i < get_btree_length(btree); i++) {
					tmpbtree = get_list_element(btree, i);
					bencode(tmpbtree, func, ctx);
				}
				func(ctx, 'e');
				break;
	case BTYPE_DICTIONARY:	func(ctx, 'd');
				for(i = 0; i < get_btree_length(btree); i++) {
					string = get_dict_attribute(btree, i);
					bencode_string(string, strlen((char *)string), func, ctx);
					tmpbtree = get_dict_value(btree, i);
					bencode(tmpbtree, func, ctx);
				}
				func(ctx, 'e');
				break;
	default:		fprintf(stderr, "bencode() error, unknown type!\n");
				exit(EXIT_FAILURE);
	}
}
