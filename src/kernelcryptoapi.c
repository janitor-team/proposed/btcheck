/*
 * kernelcryptoapi.c
 *
 * $Id$
 *
 * This file is part of the btcheck project (c) 2008-2009 distributed
 * under the GNU GPLv3 license and created by Jean Diraison
 * <jean.diraison@ac-rennes.fr>
 *
 * URL: http://sourceforge.net/projects/btcheck/
 *
 */

#include "config.h"

#ifdef WITH_KERNEL_CRYPTO_API

#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/if_alg.h>

#include "kernelcryptoapi.h"

int lkca_sha1_init(lkca_hash_ctx *ctx)
{
	ctx->sa.salg_family = AF_ALG;
	ctx->sa.salg_feat = 0;
	ctx->sa.salg_mask = 0;
	memcpy(ctx->sa.salg_type, "hash", sizeof("hash"));
	memcpy(ctx->sa.salg_name, "sha1", sizeof("sha1"));
	ctx->safd = -1;
	ctx->fd = -1;

	ctx->safd = socket(AF_ALG, SOCK_SEQPACKET, 0);
	if (ctx->safd == -1) {
		return 0;
	}

	int res = bind(ctx->safd, (struct sockaddr *) &ctx->sa, sizeof(struct sockaddr_alg));
	if (res != 0) {
		close(ctx->safd);
		ctx->safd = -1;
		return 0;
	}

	ctx->fd = accept(ctx->safd, NULL, 0);
	if (ctx->fd == -1) {
		close(ctx->safd);
		ctx->safd = -1;
		return 0;
	}
	return 1;
}

int lkca_sha1_update(lkca_hash_ctx *ctx, const void *data, unsigned long length)
{
	do {
		ssize_t len = send(ctx->fd, data, length, MSG_MORE);
		if (len == -1) 
			break;
		length -= len;
		data += len;
	} while (length != 0);

	return (length == 0 ? 1 : 0);
}

int lkca_sha1_final(unsigned char *md, lkca_hash_ctx *ctx)
{
	unsigned char *data = md;
	ssize_t length = KCA_SHA1_DIGEST_LENGTH;

	do {
		ssize_t len = recv(ctx->fd, data, length, MSG_WAITALL);
		if (len == -1)
			break;
		length -= len;
		data += len;
	} while (length != 0);

	close(ctx->fd);
	ctx->fd = -1;
	close(ctx->safd);
	ctx->safd = -1;

	return (length == 0 ? 1 : 0);
}

#endif
