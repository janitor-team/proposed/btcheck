/*
 * btcheck.h
 *
 * $Id: btcheck.h 11 2011-01-03 21:22:40Z diraison $
 *
 * This file is part of the btcheck project (c) 2008-2009 distributed
 * under the GNU GPLv3 license and created by Jean Diraison
 * <jean.diraison@ac-rennes.fr>
 *
 * URL: http://sourceforge.net/projects/btcheck/
 *
 */

#ifndef BTCHECK_BTCHECK_H_
#define BTCHECK_BTCHECK_H_

#include "config.h"

extern int Verbose;
extern int ZeroFill;

#endif
