/*
 * bencode.h
 *
 * $Id: bencode.h 3 2009-09-05 22:37:41Z diraison $
 *
 * This file is part of the btcheck project (c) 2008-2009 distributed
 * under the GNU GPLv3 license and created by Jean Diraison
 * <jean.diraison@ac-rennes.fr>
 *
 * URL: http://sourceforge.net/projects/btcheck/
 *
 */

#ifndef BTCHECK_BENCODE_H_
#define BTCHECK_BENCODE_H_

#include "btree.h"

void bencode(btree_t *btree, void (*func)(void *, int), void *ctx);

#endif
